import axios from "axios";

const instance = axios.create({
  baseURL: "http://localhost:8000",
});
const http = instance

function PredicApple(data :any) {
    return http.put('/AppleQuality', data);
  }

export default { PredicApple }
