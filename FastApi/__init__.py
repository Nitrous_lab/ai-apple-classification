from typing import Union
import joblib
from fastapi import FastAPI
from pydantic import BaseModel
import numpy as np
from fastapi.middleware.cors import CORSMiddleware
app = FastAPI()
origins = [
    "http://localhost.tiangolo.com",
    "https://localhost.tiangolo.com",
    "http://localhost",
    "http://localhost:8080",
    "http://localhost:5173",

]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

model = joblib.load('finalmodel.pkl')
quality = ['Bad','Good']
class Apple(BaseModel):
    Size: float
    Weight: float
    Sweetness :float
    Crunchiness : float
    Juiciness : float 
    Ripeness : float 
    Acidity : float
    is_offer: Union[bool, None] = None

@app.get("/")
def read_root():
    return {"Hello": "World"}

@app.put("/AppleQuality")
async def update_item(item: Apple):
    data = np.array(
    [
     [item.Size, item.Weight,item.Sweetness,item.Crunchiness,item.Juiciness,item.Ripeness,item.Acidity]
    ])
    prediction = model.predict(data)
    print(quality[prediction[0]])

    return {"prediction" : quality[prediction[0]]}